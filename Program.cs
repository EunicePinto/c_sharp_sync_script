﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Linq;


class Program
{
    static string sourceDir;
    static string replicaDir;
    static int seconds;

    static void Main(string[] args)
    {
        if (args.Length != 3)
        {
            Console.WriteLine("ERROR: Invalid number of arguments");
            Console.WriteLine("Please, try again with: 'Program <source_directory_path> <replica_folder_path> <seconds>'");
            return;
        }

        sourceDir = args[0];
        replicaDir = args[1];
        
        if (!int.TryParse(args[2], out seconds))
                {
                    Console.WriteLine("Error: Invalid value. Please, provid a valid integer value.");
                    return;
                }

        while (true)
        {
            ListFiles(sourceDir);
            CopyFiles(sourceDir, replicaDir, true);
            RemoveFiles(sourceDir, replicaDir);

            Thread.Sleep(seconds * 1000);
        }
    }


    //method that lists files by name
    public static void ListFiles(string sourceDir)
    {
         DirectoryInfo directory = new DirectoryInfo(sourceDir);

         FileInfo[] files = directory.GetFiles("*", SearchOption.AllDirectories);

         foreach (FileInfo fileInfo in files)
         {
             Console.WriteLine($"Found file: {fileInfo.Name}");
             Log($"Found file: {fileInfo.Name}"); 
         }
    }

    //method that copies files from source to replica
    public static void CopyFiles(string sourceDir, string replicaDir, bool recursive)
    {
        var dir = new DirectoryInfo(sourceDir);

        if (!dir.Exists)
            throw new DirectoryNotFoundException($"Source directory '{dir.FullName}' not found.");

        if (!Directory.Exists(replicaDir))
            Directory.CreateDirectory(replicaDir);

        foreach (FileInfo sourceFile in dir.GetFiles())
        {
            string replicaPath = Path.Combine(replicaDir, sourceFile.Name);
        
            if (!File.Exists(replicaPath) || sourceFile.LastWriteTime > File.GetLastWriteTime(replicaPath))
            {
                File.Copy(sourceFile.FullName, replicaPath, true);
                Console.WriteLine($"Copied: {sourceFile.FullName} to {replicaPath}");
                Log($"Copied: {sourceFile.FullName} to {replicaPath}");
            }
        }
        if (recursive)
        {
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                string newDestinationDir = Path.Combine(replicaDir, subDir.Name);
                CopyFiles(subDir.FullName, newDestinationDir, true);
            }
        }
    }

    //method that removes files from the replica that are not in the source
    public static void RemoveFiles(string sourceDir, string replicaDir)
    {
         DirectoryInfo sourceDirectory = new DirectoryInfo(sourceDir);
         FileInfo[] sourceFiles = sourceDirectory.GetFiles("*", SearchOption.AllDirectories);

         DirectoryInfo replicaDirectory = new DirectoryInfo(replicaDir);
         FileInfo[] replicaFiles = replicaDirectory.GetFiles("*", SearchOption.AllDirectories);

         foreach (FileInfo replicaFileInfo in replicaFiles)
         {
             string relativePath = Path.GetRelativePath(replicaDir, replicaFileInfo.FullName);
             string sourcePath = Path.Combine(sourceDir, relativePath);

             bool found = false;

             foreach (FileInfo sourceFileInfo in sourceFiles)
             {
                 if (sourceFileInfo.FullName == sourcePath)
                 {
                     found = true;
                     break;
                 }
             }

             if (!found)
             {
                 File.Delete(replicaFileInfo.FullName);
                 Console.WriteLine($"Removed: {replicaFileInfo.FullName}");
                 Log($"Removed: {replicaFileInfo.FullName}");
             }
         }
     }

    //Method that records the time and date of modification and stores each record in the log.txt file
    public static void Log(string message)
    {
        string dateTime = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss]");
        string logEntry = $"{dateTime} {message}";

        Console.WriteLine(logEntry);
        File.AppendAllText("log.txt", logEntry + Environment.NewLine);
    }
}     